class BookingOrderJob < Struct.new(:user_id, :file_url, :temp)
  include ApplicationHelper

  def perform
    puts "* Starting Booking Order Job..."
    file = File.extname(file_url) =='.xls' ? Roo::Excel.new(file_url) : Roo::Excelx.new(file_url)
    booking_orders = get_booking_content(file)
    temp.value = booking_orders
    temp.save
  rescue
    fatal_error("Invalid format!")
  end

  def get_booking_content(file)
    result = {}

    # header
    header = {
        "to":           file.row(2)[1]&.lstrip || '',
        "frm":          file.row(3)[2]&.lstrip || '',
        "tel":          file.row(2)[8].split(":")[1]&.lstrip || '',
        "job order no": file.row(2)[26].lstrip || '',
        "issued date":  file.row(3)[26]&.to_s.lstrip || ''
    }

    # import
    row = file.row(9)
    import = {
        "20":           row[2].to_i || '',
        "40":           row[3].to_i || '',
        "45":           row[4].to_i || '',
        "20R":          row[5].to_i || '',
        "40R":          row[6].to_i || '',
        "dry_total":    file.row(10)[2].to_i || '',
        "reefer_total": file.row(10)[5].to_i || '',
        "total":        file.row(8)[7].to_i || ''
    }

    # export
    row = file.row(9)
    export = {
        "morning":        row[12] || '',
        "night":          row[15] || '',
        "dry_total":      file.row(10)[12].to_i || '',
        "reefer_total":   file.row(10)[15].to_i || '',
        "total":          file.row(8)[17].to_i || ''
    }

    # empty
    row = file.row(6)
    empty = {
        "epp": row[22] || '',
        "loc": row[24] || '',
        "20'": row[26].to_i || '',
        "40'": row[27].to_i || '',
        "d/r": row[28] || ''
    }

    num_of_vessels = ((file.last_row - 14)/13)*2
    # vessels
    # case 1. the number of vessels is even
    vessels = {}
    x = 0
    (1..num_of_vessels/2).each_with_index do |idx|
      y = idx*2
      unless file.row(12 + x)[2].nil?
        vessels["#{y - 1}"] = {
            "vessel":   file.row(12 + x)[2] || '',
            "terminal": file.row(12 + x)[11] || '',
            "eta":      file.row(13 + x)[2] || '',
            "etd":      file.row(13 + x)[9] || '',
            "line":     file.row(14 + x)[2] || '',
            "date":     file.row(14 + x)[7] || '',
            "starting time": convert_starting_time_from_booking(file.row(14)[11].to_i)  || '',
            "details":{
                "urgent hot":{
                    "20":file.row(16 + x)[3] || '',
                    "40":file.row(16 + x)[4] || '',
                    "45":file.row(16 + x)[5] || '',
                    "20r":file.row(16 + x)[6] || '',
                    "40r":file.row(16 + x)[7] || '',
                    "no. beach":file.row(16 + x)[8] || '',
                    "remark":file.row(16 + x)[11] || ''
                },
                "hot":{
                    "20":file.row(17 + x)[3] || '',
                    "40":file.row(17 + x)[4] || '',
                    "45":file.row(17 + x)[5] || '',
                    "20r":file.row(17 + x)[6] || '',
                    "40r":file.row(17 + x)[7] || '',
                    "no. beach":file.row(17 + x)[8] || '',
                    "remark":file.row(17 + x)[11] || ''
                },
                "dg**hot**":{
                    "20":file.row(18 + x)[3] || '',
                    "40":file.row(18 + x)[4] || '',
                    "45":file.row(18 + x)[5] || '',
                    "20r":file.row(18 + x)[6] || '',
                    "40r":file.row(18 + x)[7] || '',
                    "no. beach":file.row(18 + x)[8] || '',
                    "remark":file.row(18 + x)[11] || ''
                },
                "reefer":{
                    "20":file.row(19 + x)[3] || '',
                    "40":file.row(19 + x)[4] || '',
                    "45":file.row(19 + x)[5] || '',
                    "20r":file.row(19 + x)[6] || '',
                    "40r":file.row(19 + x)[7] || '',
                    "no. beach":file.row(19 + x)[8] || '',
                    "remark":file.row(19 + x)[11] || ''
                },
                "normal":{
                    "20":file.row(20 + x)[3] || '',
                    "40":file.row(20 + x)[4] || '',
                    "45":file.row(20 + x)[5] || '',
                    "20r":file.row(20 + x)[6] || '',
                    "40r":file.row(20 + x)[7] || '',
                    "no. beach":file.row(20 + x)[8] || '',
                    "remark":file.row(20 + x)[11] || ''
                },
                "soc-tank":{
                    "20":file.row(21 + x)[3] || '',
                    "40":file.row(21 + x)[4] || '',
                    "45":file.row(21 + x)[5] || '',
                    "20r":file.row(21 + x)[6] || '',
                    "40r":file.row(21 + x)[7] || '',
                    "no. beach":file.row(21 + x)[8] || '',
                    "remark":file.row(21 + x)[11] || ''
                },
                "oth":{
                    "20":file.row(22 + x)[3] || '',
                    "40":file.row(22 + x)[4] || '',
                    "45":file.row(22 + x)[5] || '',
                    "20r":file.row(22 + x)[6] || '',
                    "40r":file.row(22 + x)[7] || '',
                    "no. beach":file.row(22 + x)[8] || '',
                    "remark":file.row(22 + x)[11] || ''
                },
                "rail":{
                    "20":file.row(23 + x)[3] || '',
                    "40":file.row(23 + x)[4] || '',
                    "45":file.row(23 + x)[5] || '',
                    "20r":file.row(23 + x)[6] || '',
                    "40r":file.row(23 + x)[7] || '',
                    "no. beach":file.row(23 + x)[8] || '',
                    "remark":file.row(23 + x)[11] || ''
                },
                "total":{
                    "20":file.row(24 + x)[3] || '',
                    "40":file.row(24 + x)[4] || '',
                    "45":file.row(24 + x)[5] || '',
                    "20r":file.row(24 + x)[6] || '',
                    "40r":file.row(24 + x)[7] || '',
                    "no. beach":file.row(24 + x)[8] || '',
                    "remark":file.row(24 + x)[11] || ''
                }
            }
        }
      end

      unless file.row(12 + x)[17].nil?
        vessels["#{y}"] = {
            "vessel":   file.row(12 + x)[17] || '',
            "terminal": file.row(12 + x)[26] || '',
            "eta":      file.row(13 + x)[17] || '',
            "etd":      file.row(13 + x)[24] || '',
            "line":     file.row(14 + x)[17] || '',
            "date":     file.row(14 + x)[22] || '',
            "starting time":convert_starting_time_from_booking(file.row(14 + x)[26].to_i) || '',
            "details":{
                "urgent hot":{
                    "20":file.row(16 + x)[18] || '',
                    "40":file.row(16 + x)[19] || '',
                    "45":file.row(16 + x)[20] || '',
                    "20r":file.row(16 + x)[21] || '',
                    "40r":file.row(16 + x)[22] || '',
                    "no. beach":file.row(16 + x)[23] || '',
                    "remark":file.row(16 + x)[26] || ''
                },
                "hot":{
                    "20":file.row(17 + x)[18] || '',
                    "40":file.row(17 + x)[19] || '',
                    "45":file.row(17 + x)[20] || '',
                    "20r":file.row(17 + x)[21] || '',
                    "40r":file.row(17 + x)[22] || '',
                    "no. beach":file.row(17 + x)[23] || '',
                    "remark":file.row(17 + x)[26] || ''
                },
                "dg**hot**":{
                    "20":file.row(18 + x)[18] || '',
                    "40":file.row(18 + x)[19] || '',
                    "45":file.row(18 + x)[20] || '',
                    "20r":file.row(18 + x)[21] || '',
                    "40r":file.row(18 + x)[22] || '',
                    "no. beach":file.row(18 + x)[23] || '',
                    "remark":file.row(18 + x)[26] || ''
                },
                "reefer":{
                    "20":file.row(19 + x)[18] || '',
                    "40":file.row(19 + x)[19] || '',
                    "45":file.row(19 + x)[20] || '',
                    "20r":file.row(19 + x)[21] || '',
                    "40r":file.row(19 + x)[22] || '',
                    "no. beach":file.row(19 + x)[23] || '',
                    "remark":file.row(19 + x)[26] || ''
                },
                "normal":{
                    "20":file.row(20 + x)[18] || '',
                    "40":file.row(20 + x)[19] || '',
                    "45":file.row(20 + x)[20] || '',
                    "20r":file.row(20 + x)[21] || '',
                    "40r":file.row(20 + x)[22] || '',
                    "no. beach":file.row(20 + x)[23] || '',
                    "remark":file.row(20 + x)[26] || ''
                },
                "soc-tank":{
                    "20":file.row(21 + x)[18] || '',
                    "40":file.row(21 + x)[19] || '',
                    "45":file.row(21 + x)[20] || '',
                    "20r":file.row(21 + x)[21] || '',
                    "40r":file.row(21 + x)[22] || '',
                    "no. beach":file.row(21 + x)[23] || '',
                    "remark":file.row(21 + x)[26] || ''
                },
                "oth":{
                    "20":file.row(22 + x)[18] || '',
                    "40":file.row(22 + x)[19] || '',
                    "45":file.row(22 + x)[20] || '',
                    "20r":file.row(22 + x)[21] || '',
                    "40r":file.row(22 + x)[22] || '',
                    "no. beach":file.row(22 + x)[23] || '',
                    "remark":file.row(22 + x)[26] || ''
                },
                "rail":{
                    "20":file.row(23 + x)[18] || '',
                    "40":file.row(23 + x)[19] || '',
                    "45":file.row(23 + x)[20] || '',
                    "20r":file.row(23 + x)[21] || '',
                    "40r":file.row(23 + x)[22] || '',
                    "no. beach":file.row(23 + x)[23] || '',
                    "remark":file.row(23 + x)[26] || ''
                },
                "total":{
                    "20":file.row(24 + x)[18] || '',
                    "40":file.row(24 + x)[19] || '',
                    "45":file.row(24 + x)[20] || '',
                    "20r":file.row(24 + x)[21] || '',
                    "40r":file.row(24 + x)[22] || '',
                    "no. beach":file.row(24 + x)[23] || '',
                    "remark":file.row(24 + x)[26] || ''
                }
            }

        }
      end


      x += 13
    end

    result = {
        "header": header,
        "import": import,
        "export": export,
        "empty":  empty,
        "vessels": vessels
    }

    result
  end

  def fatal_error(mes)
    temp.error    = true
    temp.err_mes  = mes
    temp.save
  end
end