class PerformanceReportJob < Struct.new(:user_id, :from_date, :to_date, :codes)
  require 'axlsx'
  require 'tempfile'
  include ApplicationHelper
  include PerformanceReportsHelper

  def perform
    puts "* Exporting the report..."
    puts "from date: #{from_date}"
    puts "to date: #{to_date}"
    out       = Outload.arel_table
    ind       = Indischarge.arel_table
    booking   = BookingOrder.arel_table

    company_id = User.find_by(id: user_id).try(:company_id)
    puts "Author: #{Company.get_name_by_id(company_id)}"

    # getting data
    outloads      = Outload.where((out[:company_id].eq(company_id)).and((out[:from].gteq(from_date).and(out[:from].lteq(to_date))).or((out[:to].gteq(from_date).and(out[:from].lteq(to_date))))))
    indischarges  = Indischarge.where((ind[:company_id].eq(company_id)).and((ind[:from].gteq(from_date).and(ind[:from].lteq(to_date))).or((ind[:to].gteq(from_date).and(ind[:from].lteq(to_date))))))
    booking_orders= BookingOrder.where((booking[:company_id].eq(company_id)).and((booking[:issued_date]+1).gteq(from_date).and((booking[:issued_date]+1).lteq(to_date))))

    trucks = codes.uniq
    trucks = User.where(role: User.roles[:trucker]).pluck(:truck_code).compact if trucks.include? ("ALL")
    @codes  = trucks
    puts "codes = #{codes}"
    # ["GPROS", "KTC", "THMPJ", "THTL", "TRTC", "TTIL", "SWT", "UNIC", "SCT"]

    # 1.1 Gathering OUTLOAD data
    temp = Temporary.new
    date_trucks = {}
    temp.item = "OUTLOAD"
    trucks&.each do |truck|
      date_trucks[truck] = [] if date_trucks[truck].nil?
      date_trucks[truck] = prepare_report_data(outloads, truck)
    end
    temp.value = date_trucks                                                          # save outload in temporary table
    # outload_temp_id = temp.id if temp.save                                                     # temp id for retrieving
    outloads_temp = date_trucks

    # 1.2 Gathering INDISCHARGE data
    temp = Temporary.new
    date_trucks = {}
    temp.item = "INDISCHARGE"
    trucks&.each do |truck|
      date_trucks[truck] = [] if date_trucks[truck].nil?
      date_trucks[truck] = prepare_report_data(indischarges, truck)
    end
    temp.value = date_trucks                                                      # save indischarge in temporary table
    # indischarge_temp_id = temp.id if temp.save                                                 # temp id for retrieving
    indischarges_temp = date_trucks

    # 1.3 Gathering BOOKING ORDER data
    temp = Temporary.new
    order_temp = {}
    companies = [
        ["PROSPERRITY INTERNATIONAL", "GPROS"],
        ["KRUNGTHEP", "KTC"],
        ["MPJ", "THMPJ"],
        ["T.LINE", "THTL"],
        ["FUJITRAN", "TRTC"],
        ["THAITRANS", "TTIL"],
        ["SIAM", "SCT"],
        ["UNICORN", "UNIC"],
        ["SOR WATTANA", "SWT"]
    ]

    # booking_orders was sorted as descending order of issued date
    truck_code = nil
    dte_temp = '0000-00-00'

    orders = []
    booking_orders&.each do |bo|
      dte_temp = bo.issued_date&.strftime("%d/%m/%Y") if bo.issued_date != dte_temp                            # clarify based on issued date

      # detect truck company's code
      to = bo.header["header"]["to"]&.upcase
      companies&.each do |com|
        truck_code = companies.assoc(com[0])[1] if to.include? com[0]
      end

      unless truck_code.nil?
        dte_temp = (convert_date_dmy(dte_temp) + 1).strftime("%d/%m/%Y")
        orders << {
            truck: truck_code,
            date: dte_temp,
            details: {
                header:   bo.header,
                details:  bo.details
            }
        }
      end
    end

    # 2. REPORT ROWS ==================================================================

    # 2.1 getting all date
    @row_date = []
    trucks&.each do |truck|
      @row_date += outloads_temp[truck]&.keys || "01-01-1000" unless outloads_temp[truck].nil?
      @row_date += indischarges_temp[truck]&.keys || "01-01-1000" unless indischarges_temp[truck].nil?
    end
    @row_date = (@row_date + booking_orders.pluck(:issued_date).collect{ |d| (d+1).strftime("%d/%m/%Y")}).uniq

    # replace nil by "01-01-1000" and sort in ascending order
    @row_date = @row_date.collect{ |e|
      unless e.nil?
        e
      else
        e = "01-01-1000"
      end
    }.sort{ |x,y|
      begin
        convert_date_dmy(x) <=> convert_date_dmy(y)
      rescue
        e = convert_date_dmy("01-01-1000")
      end
    }

    report_rows = []
    @row_date&.each do |dte|
      trucks&.each do |truck|
        next if order_temp[truck].nil? && outloads_temp[truck].nil? && indischarges_temp[truck].nil?
        # initialize variables
        impt_plan_dry   = 0
        impt_plan_rf    = 0
        impt_plan_total = 0
        impt_actual_20  = 0
        impt_actual_40  = 0
        impt_actual_rf  = 0
        impt_total      = 0
        impt_disc1n2    = 0
        impt_result1n2  = 0

        expt_plan_dry   = 0
        expt_plan_rf    = 0
        expt_plan_total = 0
        expt_actual_20  = 0
        expt_actual_40  = 0
        expt_actual_rf  = 0
        expt_total      = 0
        expt_disc1n2    = 0
        expt_result1n2  = 0

        total_actual_trip = 0

        perf_single_vol = 0
        perf_round_vol  = 0
        perf_single_per = 0
        perf_round_per  = 0

        # BEGIN EXPORT ====================================================
        # 1. Plan - Job Order
        # unless [truck].nil?
        # order_temp[truck][dte]&.each do |order|
        orders.each do |order|
          if order[:truck] == truck && order[:date] == dte
            expt_plan_dry   += order[:details][:header]["export"]["dry_total"] unless order[:details][:header]["export"]["dry_total"].nil?
            expt_plan_rf    += order[:details][:header]["export"]["reefer_total"] unless order[:details][:header]["export"]["reefer_total"].nil?
            expt_plan_total += order[:details][:header]["export"]["total"] unless order[:details][:header]["export"]["total"].nil?
          end
        end
        # end

        # 2. Actual - Outload
        unless outloads_temp[truck].nil?
          outloads_temp[truck][dte]&.each do |outload|
            expt_actual_20 += 1 if container_size_20 outload["Size/Type"]
            expt_actual_40 += 1 if container_size_40 outload["Size/Type"]
            expt_actual_rf += 1 if container_size_rf outload["Size/Type"]
          end
        end
        expt_total    = expt_actual_20 + expt_actual_40 + expt_actual_rf
        expt_disc1n2  = expt_total - expt_plan_total
        expt_result1n2= expt_total/expt_plan_total if expt_plan_total != 0
        # END EXPORT

        # BEGIN IMPORT ====================================================
        # 1. Plan - Job Order
        orders.each do |order|
          if order[:truck] == truck && order[:date] == dte
            impt_plan_dry   += order[:details][:header]["import"]["dry_total"] unless order[:details][:header]["import"]["dry_total"].nil?
            impt_plan_rf    += order[:details][:header]["import"]["reefer_total"] unless order[:details][:header]["import"]["reefer_total"].nil?
            impt_plan_total += order[:details][:header]["import"]["total"] unless order[:details][:header]["import"]["total"].nil?
          end
        end

        # 2. Actual - Outload
        unless indischarges_temp[truck].nil?
          indischarges_temp[truck][dte]&.each do |indischarge|
            impt_actual_20 += 1 if container_size_20 indischarge["Size/Type"]
            impt_actual_40 += 1 if container_size_40 indischarge["Size/Type"]
            impt_actual_rf += 1 if container_size_rf indischarge["Size/Type"]
          end
        end
        impt_total    = impt_actual_20 + impt_actual_40 + impt_actual_rf
        impt_disc1n2  = impt_total - impt_plan_total
        impt_result1n2= impt_total/impt_plan_total if impt_plan_total != 0
        # END IMPORT

        total_actual_trip = expt_total + impt_total

        # setting date for single row
        report_rows << {
            "date": dte || "01-01-1000",
            "truck": truck,

            "expt_plan_dry":    expt_plan_dry,
            "expt_plan_rf":     expt_plan_rf,
            "expt_plan_total":  expt_plan_total,

            "expt_actual_20":   expt_actual_20,
            "expt_actual_40":   expt_actual_40,
            "expt_actual_rf":   expt_actual_rf,
            "expt_total":       expt_total,
            "expt_disc1n2":     expt_disc1n2,
            "expt_result1n2":   "#{expt_result1n2*100}%",

            "impt_plan_dry":    impt_plan_dry,
            "impt_plan_rf":     impt_plan_rf,
            "impt_plan_total":  impt_plan_total,

            "impt_actual_20":   impt_actual_20,
            "impt_actual_40":   impt_actual_40,
            "impt_actual_rf":   impt_actual_rf,
            "impt_total":       impt_total,
            "impt_disc1n2":     impt_disc1n2,
            "impt_result1n2":   "#{impt_result1n2*100}%",

            "total_actual_trip":total_actual_trip,
            "perf_single_vol":  perf_single_vol,
            "perf_round_vol":   perf_round_vol,
            "perf_single_per":  perf_single_per,
            "perf_round_per":   perf_round_per
        }
      end
    end

    generateReport report_rows
  end

  # for outload and indischarge data:
  # dta: outloads or indischarges
  # truck: GPROS, KTC...
  def prepare_report_data(dta, truck)
    date_dta = {}
    dta&.each do |dta1|
      dta1.details[truck]&.each do |load|
        dte = load["Incoming Date/Time"].split(" ")[0]
        date_dta[dte] = [] if date_dta[dte].nil?
        date_dta[dte] << load
      end
    end
    date_dta
  end

  def generateReport rows
    package = Axlsx::Package.new
    workbook = package.workbook

    workbook.add_worksheet(name: "DailyBasic") do |sheet|
      workbook.styles do |s|
        # date = s.add_style(:format_code => "[$-409]d-mmm-yyyy hh:mm AM/PM;@")
        # merging rows
        sheet.merge_cells("B1:Z1")
        sheet.merge_cells("D2:L2")
        sheet.merge_cells("M2:U2")
        sheet.merge_cells("W2:Z2")
        sheet.merge_cells("D3:F3")
        sheet.merge_cells("G3:I3")
        sheet.merge_cells("M3:O3")
        sheet.merge_cells("P3:R3")

        # merging columns
        sheet.merge_cells("J3:J4")
        sheet.merge_cells("K3:K4")
        sheet.merge_cells("T3:T4")
        sheet.merge_cells("S3:S4")
        sheet.merge_cells("V2:V4")
        sheet.merge_cells("A2:A4")
        sheet.merge_cells("B2:B4")
        sheet.merge_cells("C2:C4")

        width = [10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10]
        style = sheet.styles.add_style(:border => {:style => :thin, :color => "FF333333"}, :sz => 18,  :font_name => 'Arial', :alignment => { :horizontal => :center, :vertical => :center, :wrap_text => true}, :widths => width)
        style1 = sheet.styles.add_style(:border => {:style => :thin, :color => "FF333333"},:bg_color => "E87E04", :fg_color => "FFFFFF", :sz => 11,  :font_name => 'Arial', :alignment => { :horizontal => :center, :vertical => :center, :wrap_text => true, :widths => [:ignore]}, )
        style2 = sheet.styles.add_style(:border => {:style => :thin, :color => "FF333333"},:bg_color => "4B77BE", :fg_color => "FFFFFF", :sz => 11,  :font_name => 'Arial', :alignment => { :horizontal => :center, :vertical => :center, :wrap_text => true})
        style3 = sheet.styles.add_style(:border => {:style => :thin, :color => "FF333333"},:bg_color => "f3c200", :fg_color => "FFFFFF", :sz => 11,  :font_name => 'Arial', :alignment => { :horizontal => :center, :vertical => :center, :wrap_text => true})
        normal = sheet.styles.add_style(:border => {:style => :thin, :color => "FF333333"}, :alignment => { :horizontal => :center, :vertical => :center, :wrap_text => true}, :widths => width)
        style4 = sheet.styles.add_style(:alignment => { :horizontal => :right, :vertical => :top, :wrap_text => false})

        # adding header
        sheet.add_row([nil, "Monthly Trucking Performance Report #{from_date.to_date.strftime("%d, %B %Y")} - #{to_date.to_date.strftime("%d, %B %Y")}", nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil], :style => style)
        sheet.add_row(["", "Date", "Truck", "EXPORT Volume (Unit)", "IMPORT Volume (Unit)", "Total Actual Trip", "PERFORMANCE", nil, nil, nil, nil, nil, "IMPORT Volume (Unit)", "IMPORT Volume (Unit)", "Total Actual Trip", "PERFORMANCE", nil, nil, nil, nil, nil, "Total Actual Trip", "PERFORMANCE", nil, nil, nil], :style => style1)
        sheet.add_row(["1.Plan", "Total", "Disc1&2", "1.Plan", "Result", "1.Plan", "2.Actual", "Total", "Disc1&2", "Total", "Disc1&2", "Result", "1.Plan", "Result", "1.Plan", "2.Actual", "Total", "Disc1&2", "Total", "Disc1&2", "Result", nil, "Single", "Round", "Single", "Round"], :style => style2)
        sheet.add_row([nil, nil, nil, "Dry", "RF", "Total", "20'", "40'", "RF", nil, nil, "1&2", "Dry", "RF", "Total", "20'", "40'", "RF", nil, nil, "1&2", nil, "Vol.", "Vol.", "%", "%"], :style => style3)

        sheet.column_info.first.width = 5

        dte     = ''
        first   = 4
        second  = 0
        first_date = ''

        rows&.each do |dta|
          row = dta.values
          unless row[0].nil?
            begin
              next if (convert_date_dmy(row[0]) < from_date) || (convert_date_dmy(row[0]) > to_date)
            rescue
              next
            end
          else
            next
          end

          if (row[0] == dte)
            row[0] = nil
          else
            dte      = row[0]
            first   += 1
            second   = first + (@codes.size - 1)
            sheet.merge_cells("A#{first}:A#{second}")

            begin
              first_date = dte&.to_date&.strftime("%A")
              if first_date == "01-01-1000"
                first_date = ''
              end
            rescue
              first_date = ''
            end
          end

          # puts "first = #{first} ; second = #{second}"

          sheet.add_row(
              [
                  first_date,
                  row[0],
                  row[1],
                  row[2],
                  row[3],
                  row[4],
                  row[5],
                  row[6],
                  row[7],
                  row[8],
                  row[9],
                  row[10],
                  row[11],
                  row[12],
                  row[13],
                  row[14],
                  row[15],
                  row[16],
                  row[17],
                  row[18],
                  row[19],
                  row[20],
                  row[21],
                  row[22],
                  row[23],
                  row[24]
              ],
              :style => style4, :widths => width

          # :style => normal
          )

          first = second
        end

      end
    end
    s = package.to_stream()

    # creating and writing data to a file
    file_path = "#{Rails.root}/public/downloads/performance_report_#{user_id}_#{Time.zone.now.to_i}.xlsx"
    File.open(file_path, 'w+') { |f| f.write(s.read) }

    # upload file which was created to S3 Amazon
    file_s3 = ImportFile.upload_internal(file_path, user_id)

    # deleting temp file
    File.delete(file_path)

    # nofifying the user who created this report
    message = "#{file_s3.url};The report was just exported"
    notification = Notification.create_notification( nil, message, Notification.gsubject(Notification::EXP_REPORT, nil))
    Notification.inform_forwarder(notification, user_id)
  end

  def fatal_error
    temp.error    = true
    temp.err_mes  = "Invalid Format!"
    temp.save
  end
end