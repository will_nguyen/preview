class IndischargeJob < Struct.new(:user_id, :file_url, :temp)
  include ApplicationHelper

  def perform
    puts "* Starting Indischarge Job..."
    file = File.extname(file_url) =='.xls' ? Roo::Excel.new(file_url) : Roo::Excelx.new(file_url)

    indischarges = get_outload_content(file)
    temp.value = indischarges
    temp.save
  rescue
    fatal_error("Invalid format!")
  end

  def get_outload_content(file)
    header = {
        "1": file.sheet('Summary')&.row(1)[0].to_s || '',
        "2": file.sheet('Summary')&.row(2)[0].to_s || '',
        "3": file.sheet('Summary')&.row(3)[0].to_s || ''
    }

    # Sheet's content
    gpros   = get_outload_content_by_sheet(file, "GPROS", 6)
    ktc     = get_outload_content_by_sheet(file, "KTC",   6)
    thmpj   = get_outload_content_by_sheet(file, "THMPJ", 6)
    swt     = get_outload_content_by_sheet(file, "SWT",   6)
    thtl    = get_outload_content_by_sheet(file, "THTL",  6)
    trtc    = get_outload_content_by_sheet(file, "TRTC",  6)
    ttil    = get_outload_content_by_sheet(file, "TTIL",  6)
    unic    = get_outload_content_by_sheet(file, "UNIC",  6)
    sct     = get_outload_content_by_sheet(file, "SCT",   6)
    summary = get_outload_content_by_sheet(file, "Summary", 6)

    result  = {
        "header": header,
        "GPROS":  gpros,
        "KTC":    ktc,
        "THMPJ":  thmpj,
        "SWT":    swt,
        "THTL":   thtl,
        "TRTC":   trtc,
        "TTIL":   ttil,
        "UNIC":   unic,
        "SCT":    sct,
        "Summary": summary
    }
    result
  end

  def get_outload_content_by_sheet(file, sheet = '', first_row = 0)
    res = []
    unless sheet == "Summary"
      begin
        (first_row..file.sheet(sheet)&.last_row)&.each_with_index do |i|
          row = file.sheet(sheet).row(i)
          content = {
              "Container No":       row[1]  || '',
              "Line":               row[2]  || '',
              "Size/Type":          row[3]  || '',
              "Status":             row[4]  || '',
              "Incoming Date/Time": row[5]  || '',
              "Vessel/Voyage":      row[6]  || '',
              "Terminal":           row[7]  || '',
              "Seal No.":           row[8]  || '',
              "Head":               row[9]  || '',
              "Truck License":      row[10] || '',
              "Booking No.":        row[11] || ''
          }
          res << content
        end
      rescue
      end
    else
      # Summary Sheet
      (first_row..file.sheet(sheet).last_row).each_with_index do |i|
        row = file.sheet(sheet).row(i)
        content = {
            "#{row[0]}": {
                "20'": {
                    "GP":     row[1].to_i || 0,
                    "RF":     row[2].to_i || 0,
                    "TK":     row[3].to_i || 0,
                    "OT":     row[4].to_i || 0,
                    "FL":     row[5].to_i || 0,
                    "2X20'":  row[6].to_i || 0,
                    "Total":  row[7].to_i || 0
                },

                "40'": {
                    "GP":     row[8].to_i   || 0,
                    "HQ":     row[9].to_i   || 0,
                    "OT":     row[10].to_i  || 0,
                    "FL":     row[11].to_i  || 0,
                    "RQ":     row[12].to_i  || 0,
                    "Total":  row[13].to_i  || 0
                },
                "45'":        row[14].to_i  || 0,
                "Total Unit": row[15].to_i  || 0
            }
        }
        res << content
      end
    end
    res
  end

  def fatal_error(mes)
    temp.error    = true
    temp.err_mes  = mes
    temp.save
  end
end