require 'task_helpers/tasks_helper'
desc "Inactive work order with cron on certain time"
task inactivate_work_order: :environment do
  # CREATE_WO       = "Ostrich : New order"
  # TRUCKER_BIDDED  = "Ostrich : Order is Bidded"
  # WINNER_ASSIGNED = "Ostrich : Container is assigned"
  # WINNER_ACCEPTED = "Ostrich : Trucker accepted"
  # EDIT_WO         = "Ostrich : Order change"
  # NO_WINNER       = "Ostrich : Please manually assign"
  # CANCEL_WO       = "Ostrich : Order was cancelled"
  # EXP_REPORT      = "Ostrich : Report exported"
  # NO_BIDDER       = "Ostrich : No bidder"
  # LOST_BIDDING    = "Ostrich : WO# was won by other truckers"
  # REMARK_UPDATED  = "Ostrich : Remarks was updated"
  # DOC_REPLACED    = "Ostrich : Document updated"

  WorkOrder.are_ready_to_inactive.find_each do |wo|
    # inactivating manual work order
    if wo.manual
      TasksHelper.inactivate_single_work_order wo

      # notify
      message = "#{wo.request_no}: This Work Order was expired. Please manually assign to Truckers"
      notification = Notification.create_notification(wo.id, message, "Ostrich : No bidder - #{wo.request_no}")
      Notification.inform_forwarder(notification, wo.user_id)
      next
    end

    puts "rake inactivate_work_order:#{wo.request_no} ======================================================"
    manual = false

    wo.containers&.each do |container|
      winners = []
      container_type = container.get_type
      container_quotations = Carrier::Quotation.where(container_id: container.id).order(price: :asc)
      # Verifying quotations
      puts "#{wo.request_no}.quotation.size: #{container_quotations.size}"
      if container_quotations.size < 1
        manual = true
        message = "#{wo.request_no}: There is no bid on #{container_type} container"
        notification = Notification.create_notification(wo.id, message, "Ostrich : No bidder - #{wo.request_no}")
        Notification.inform_forwarder(notification, wo.user_id)
      else
        #---------------------------------------------
        # VALIDATING for every container AND NOTIFYING
        #---------------------------------------------
        # if wo is "auto reject"
        puts "#{wo.request_no}.auto reject: #{wo.auto_reject}"
        if wo.auto_reject
          container_quotations&.each do |quotation|
            # updating quotation's rejected status
            if quotation.price > container.price
              quotation.rejected = true
              quotation.save
            end
          end
        end

        # getting rid of the quotations which were rejected and having price > requested rate
        container_quotations = container_quotations.where('rejected = ? and price <= ?', false, container.price)
        next if container_quotations.first.nil?

        begin
          # verifying whether having the winner
          quotation_minval = container_quotations.first.price
          puts "quotation_minval = #{quotation_minval}"
          puts "#{wo.request_no}.requested_rate: #{container.price}"
          # checking if quotation_minval <= requested rate
          if quotation_minval <= container.price
            # if having two quotations have the same min val
            count = container_quotations.where(price: quotation_minval).size
            if count > 1
              message = "#{wo.request_no}: There are #{count} truckers having the same lowest prices on #{container_type} container"
              notification = Notification.create_notification(wo.id, message, "Ostrich : Please manually assign - #{wo.request_no}")
              Notification.inform_forwarder(notification, wo.user_id)
              manual = true
            end
          else
            manual = true
            message = "#{wo.request_no}: Please manually assign the winner as all the bidder submitted higher rate than requested"
            notification = Notification.create_notification(wo.id, message, "Ostrich : Please manually assign - #{wo.request_no}")
            Notification.inform_forwarder(notification, wo.user_id)
          end
        rescue Exception => e
          puts "////////////////////// ERROR /////////////////////"
          puts e.message
          puts "work_order_id: #{wo.id}"
          puts "container_id: #{container.id}"
        end
      end

      # Getting an array of truckers will be assigned in ASC order. Structure: [company_id, unit, price, container_id]
      # 0: company_id | 1: unit | 2: price | 3: container_id
      quots = container_quotations.collect{|q|[q.company_id, q.unit, q.price, q.container_id]}.sort {|a,b| a[2] <=> b[2]}

      puts "manual = #{manual}"
      if manual
        # changing wo to manual
        wo.manual = true

        message = "#{wo.request_no}: Please manually allocate #{wo.is_trunk ? wo.get_trunk_container_type << ' container' : 'this job'}"
        notification = Notification.create_notification(wo.id, message, "Ostrich : Please manually assign - #{wo.request_no}")
        Notification.inform_forwarder(notification, wo.user_id)
      else
        # --------------------------
        # ASSIGNING JOBs TO TRUCKERs
        # --------------------------
        quots&.each do |quot|
          puts "proceed quotation: #{quot}"
          # checking assigned unit
          assigned_units = Container.find_by(id: quot[3]).get_assigned_units || 0
          # verifying the quantity
          unit_to_assign = quot[1]
          container_units = Container.find_by(id: quot[3]).try(:unit)
          remaining_units = container_units - assigned_units < 0 ? 0 : container_units - assigned_units
          puts "#{container_type}.vailable_units: #{remaining_units}"
          unit_to_assign = remaining_units if quot[1] >= remaining_units
          unless unit_to_assign == 0
            assignment = Assignment.new(company_id: quot[0], # trucker who is assigned
                                        work_order_id: wo.id,
                                        user_id: wo.user_id, # shipper who assign
                                        container_id: quot[3],
                                        unit: unit_to_assign,
                                        price: quot[2])
            if assignment.save
              puts "assignment #{assignment.id} was created. assigned #{assignment.unit} #{container_type} to #{Company.get_name_by_id(assignment.company_id)}"
              message = "#{wo.request_no}:#{container_type} was assigned to #{Company.find_by(id: quot[0]).try(:name)}"
              notification = Notification.create_notification(wo.id, message, "Ostrich : Container is assigned - #{wo.request_no}")
              Notification.inform_forwarder(notification, wo.user_id)
            end
            quot[1] = unit_to_assign
            winners << quot
          end
        end

        # Changing container's status
        container.update(status: Container.statuses[:assigned])
      end

      # NOTIFY THE WINNERS
      puts "Winner for #{container_type} container: #{winners}"
      winners&.each do |winner|
        puts "#{container_type}.winner: #{winner}"
        # winner[] - 0: company_id | 1: unit | 2: price | 3: container_id
        # inform trucker
        if !manual
          message = "#{wo.request_no}:#{container_type}x#{winner[1]} was assigned to you"
          notification = Notification.create_notification(wo.id, message, "Ostrich : Container is assigned - #{wo.request_no}")
          company_ids = []
          company_ids << winner[0]

          # notifying the winner that he got this job
          Notification.inform_trucker(notification, company_ids, true)
        end
      end

      # NOTIFY TRUCKERS WHO BIDDED JOB FAIL
      # nofifying the others that they lost this job
      others = []
      container_quotations&.each do |quotation|
        others << quotation.company_id if winners.assoc(quotation.company_id).nil?
      end
      message = "#{wo.request_no}:#{container_type} was won by another truckers"
      notification = Notification.create_notification(wo.id, message, "Ostrich : #{wo.request_no} was won by other truckers")
      Notification.inform_trucker(notification, others, false)
    end # each container
    # inactive work order
    if @winner.nil?
      wo.update_attributes(active: false, status: WorkOrder.statuses[:quoted])
    else
      wo.update_attributes(active: false, status: WorkOrder.statuses[:assigned])
    end
  end
end

